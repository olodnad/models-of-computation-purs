module Test.WhileLoop
  ( testsWhileLoop
  )
  where

import Prelude
import ProceduralModels.Common (Const(..), Variable(..))
import ProceduralModels.LoopWhile (LWProgram(..))
import Data.Either (Either(..), isLeft)
import Data.List (List(..), (:))
import Effect (Effect)
import Effect.Class.Console (log)
import MetaModel (run, runBound)
import Parsing (runParser)
import ProceduralModels.Parsing (lwProgramP)
import ProceduralModels.Printing (printWhileLoopProgram)
import Test.Assert (assert, assertEqual)
import Test.QuickCheck (quickCheck)
import Test.Test (TestMsg, runTest, small)

testsWhileLoop :: Effect Unit
testsWhileLoop = do
    log "---------------------------------"
    log "Property tests"
    log "---------------------------------"
    runTest addLoopT
    runTest mulLoopT
    runTest addWhileT
    runTest foreverWhileT
    log "---------------------------------"
    log "Unit tests"
    log "---------------------------------"
    runTest parseAddLoop
    runTest parseAddWhile
    runTest parsePrintMulLoop
    runTest parsePrintAddWhile
    runTest readToEnd

--------------------------------------------------------------------------------
--# Property tests
--------------------------------------------------------------------------------

addLoop :: LWProgram
addLoop =
    Seq (AssignPlus (X 0) (X 1) (C 0))
        (Loop (X 2)
            (AssignPlus (X 0) (X 0) (C 1))
        )

mulLoop :: LWProgram
mulLoop =
    (Loop (X 1)
        (Loop (X 2)
            (AssignPlus (X 0) (X 0) (C 1))
        )
    )

addWhile :: LWProgram
addWhile =
    Seq (AssignPlus (X 0) (X 1) (C 0))
        (While (X 2)
            ( Seq (AssignPlus (X 0) (X 0) (C 1))
                (AssignMinus (X 2) (X 2) (C 1))
            )
        )

foreverWhile :: LWProgram
foreverWhile = Seq
    (AssignPlus (X 1) (X 0) (C 1))
    (While (X 1) (AssignPlus (X 0) (X 0) (C 1)))

addLoopT :: TestMsg
addLoopT =
    { test: quickCheck $ \x y -> run addLoop (small 1000 x: small 1000 y:Nil)
        == small 1000 x + small 1000 y
    , msg: "Addition in Loop"
    }

mulLoopT :: TestMsg
mulLoopT =
    { test: quickCheck $ \x y -> run mulLoop (small 100 x: small 100 y:Nil)
        == small 100 x * small 100 y
    , msg: "Multiplication in Loop"
    }

addWhileT :: TestMsg
addWhileT =
    { test: quickCheck $ \x y -> run addWhile (small 1000 x: small 1000 y:Nil)
        == small 1000 x + small 1000 y
    , msg: "Addition in While"
    }

foreverWhileT :: TestMsg
foreverWhileT =
    { test: quickCheck $
        \x ys -> isLeft $ runBound foreverWhile (small 1000 x) ys
    , msg: "Forever While"
    }

--------------------------------------------------------------------------------
--# Unit tests (parsing)
--------------------------------------------------------------------------------

parseAddLoop :: TestMsg
parseAddLoop =
    { test: assertEqual
        { actual : runParser addStr lwProgramP
        , expected : Right addLoop
        }
    , msg: "Parsing addition in Loop"
    }
    where
        addStr =
            "x0 = x1 + 0;\n\
            \loop x2 do\n\
            \  x0 = x0 + 1\n\
            \end"

parseAddWhile :: TestMsg
parseAddWhile =
    { test: assertEqual
        { actual : runParser addStr lwProgramP
        , expected : Right addWhile
        }
    , msg: "Parsing addition in While"
    }
    where
        addStr =
            "x0 = x1 + 0;\n\
            \while x2 > 0 do\n\
            \  x0 = x0 + 1;\n\
            \  x2 = x2 - 1\n\
            \end"

parsePrintMulLoop :: TestMsg
parsePrintMulLoop =
    { test: assertEqual
        { actual : runParser (printWhileLoopProgram mulLoop) lwProgramP
        , expected : Right mulLoop
        }
    , msg: "Parsing and printing multiplication in Loop"
    }

parsePrintAddWhile :: TestMsg
parsePrintAddWhile =
    { test: assertEqual
        { actual : runParser (printWhileLoopProgram addWhile) lwProgramP
        , expected : Right addWhile
        }
    , msg: "Parsing and printing addition in While"
    }

readToEnd :: TestMsg
readToEnd =
    { test: assert $ isLeft $ runParser faulty lwProgramP
    , msg: "Reading to the end of the program"
    }
    where
        faulty = "x0 = x1 + 0\n\
        \ garbage"