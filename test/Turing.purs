module Test.Turing where

import Prelude
import Common.Auxiliary (Triple(..), strToCharList)
import Data.Either (Either(..), isRight, isLeft)
import Data.Int (toStringAs, binary)
import Data.List (List(..))
import Data.Map as M
import Data.Set as S
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Class.Console (log)
import MachineModels.Common (Acceptance(..))
import MachineModels.Parsing (tmAccP, tmCalcP)
import MachineModels.Printing (tmCalcPrint, tmAccPrint)
import MachineModels.TuringM (Direction(..), TuringMachineCalc(..), TuringMachineAcceptor(..))
import MetaModel (run, runBound)
import Parsing (parseErrorMessage, runParser)
import Test.Assert (assertEqual, assertTrue)
import Test.QuickCheck (quickCheck)
import Test.Test (TestMsg, runTest, small)

testTuring :: Effect Unit
testTuring = do
    log "---------------------------------"
    log "Property tests"
    log "---------------------------------"
    runTest addTMT
    runTest mod3AccT
    runTest turingForeverT
    log "---------------------------------"
    log "Unit tests"
    log "---------------------------------"
    runTest parsePrintRoundtripTMC
    runTest parsePrintRoundtripTMA
    runTest parse

--------------------------------------------------------------------------------
--# Property tests
--------------------------------------------------------------------------------

addTM :: TuringMachineCalc Char String
addTM = TMC
    { initialState: "MR"
    , blank: '_'
    , transitions: M.fromFoldable
        [ Tuple (Tuple "MR"    '+')  (Triple "Sub1"  '+' R)
        , Tuple (Tuple "MR"    '0')  (Triple "MR"    '0' R)
        , Tuple (Tuple "MR"    '1')  (Triple "MR"    '1' R)
        , Tuple (Tuple "MR"    '_')  (Triple "MR"    '_' R)
        , Tuple (Tuple "Sub1"  '_')  (Triple "Sub2"  '_' L)
        , Tuple (Tuple "Sub1"  '0')  (Triple "Sub1"  '0' R)
        , Tuple (Tuple "Sub1"  '1')  (Triple "Sub1"  '1' R)
        , Tuple (Tuple "Sub2"  '0')  (Triple "Sub2"  '0' L)
        , Tuple (Tuple "Sub2"  '1')  (Triple "Sub3"  '0' R)
        , Tuple (Tuple "Sub2"  '+')  (Triple "Clean" '_' R)
        , Tuple (Tuple "Clean" '+')  (Triple "Clean" '_' R)
        , Tuple (Tuple "Clean" '0')  (Triple "Clean" '_' R)
        , Tuple (Tuple "Clean" '1')  (Triple "Clean" '_' R)
        , Tuple (Tuple "Sub3"  '0')  (Triple "Sub3"  '1' R)
        , Tuple (Tuple "Sub3"  '_')  (Triple "ML"    '_' L)
        , Tuple (Tuple "ML"    '+')  (Triple "Add1"  '+' L)
        , Tuple (Tuple "ML"    '0')  (Triple "ML"    '0' L)
        , Tuple (Tuple "ML"    '1')  (Triple "ML"    '1' L)
        , Tuple (Tuple "ML"    '_')  (Triple "ML"    '_' L)
        , Tuple (Tuple "Add1"  '1')  (Triple "Add1"  '0' L)
        , Tuple (Tuple "Add1"  '0')  (Triple "MR"    '1' R)
        , Tuple (Tuple "Add1"  '+')  (Triple "MR"    '1' R)
        , Tuple (Tuple "Add1"  '_')  (Triple "MR"    '1' R)
        ]
    }

accMod3TM :: TuringMachineAcceptor Char String
accMod3TM = TMA
    { baseTm:
        { initialState: "A"
        , blank: '_'
        , transitions: M.fromFoldable
            [ Tuple (Tuple "A" '0') (Triple "A" '_' R)
            , Tuple (Tuple "A" '1') (Triple "B" '_' R)
            , Tuple (Tuple "B" '0') (Triple "C" '_' R)
            , Tuple (Tuple "B" '1') (Triple "A" '_' R)
            , Tuple (Tuple "C" '0') (Triple "B" '_' R)
            , Tuple (Tuple "C" '1') (Triple "C" '_' R)
            ]
        }
    , acceptingStates: S.singleton "A"
    }

turingForever :: TuringMachineCalc Int Int
turingForever = TMC
    { initialState: 0
    , blank: 0
    , transitions: M.singleton (Tuple 0 0) (Triple 0 0 R)
    }

addTMT :: TestMsg
addTMT =
    { test: quickCheck $ \x y -> run addTM
        (strToCharList
            (toBinary (small 1000 x)
            <> "+"
            <> toBinary (small 1000 y)
            )
        )
        == strToCharList (toBinary (small 1000 x + small 1000 y))
    , msg: "Addition in binary"
    }
    where
        toBinary :: Int -> String
        toBinary = toStringAs binary

mod3AccT :: TestMsg
mod3AccT =
    { test: quickCheck $
        \x -> case run accMod3TM (strToCharList (toBinary (small 1000 x))) of
            Accept _ -> small 1000 x `mod` 3 == 0
            _ -> small 1000 x `mod` 3 /= 0
    , msg: "Accepting numbers mod 3 in binary"
    }
    where
        toBinary :: Int -> String
        toBinary = toStringAs binary

turingForeverT :: TestMsg
turingForeverT =
    { test: quickCheck $
        \n -> isLeft (runBound turingForever (small 100 n)  Nil)
    , msg: "Forever Turing"
    }

--------------------------------------------------------------------------------
--# Unit tests (parsing)
--------------------------------------------------------------------------------

parsePrintRoundtripTMC :: TestMsg
parsePrintRoundtripTMC =
    { test: assertEqual
        { actual: case runParser mStr tmCalcP of
            Left e -> Left $ parseErrorMessage e
            Right tm -> Right (tmCalcPrint tm)
        , expected: Right mStr
        }
    , msg: "Parse-print roundtrip (TMC)"
    }
    where
        mStr :: String
        mStr = tmCalcPrint addTM

parsePrintRoundtripTMA :: TestMsg
parsePrintRoundtripTMA =
    { test: assertEqual
        { actual: case runParser mStr tmAccP of
            Left e -> Left $ parseErrorMessage e
            Right tm -> Right (tmAccPrint tm)
        , expected: Right mStr
        }
    , msg: "Parse-print roundtrip (TMA)"
    }
    where
        mStr :: String
        mStr = tmAccPrint accMod3TM

parse :: TestMsg
parse =
    { test: assertTrue $ isRight $ runParser mStr tmCalcP
    , msg: "Admissible syntax styles"
    }
    where
        mStr :: String
        mStr =
            "initial state: MR;\n"
            <> "blank: '_';\n"
            <> "transitions:\n"
            <> "  (MR, +) -> (Sub1, +, R);\n"
            <> "  (MR, 0) -> (MR, 0, R);\n"
            <> "  (MR, 1) -> (MR, 1, R) ;\n"
            <> "  ( MR\n"
            <> "  , _) -> (MR, _, R);\n"
            <> "  (Sub1, _) -> (Sub2, _, L);\n"
            <> "(Sub1,0)->(Sub1,0,R);\n"
            <> "  (Sub1, 1) -> (Sub1, 1, R);\n"
            <> "  (Sub2, 0) -> (Sub2, 0, L)"
