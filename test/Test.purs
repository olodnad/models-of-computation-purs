module Test.Test where

import Prelude
import Common.Auxiliary (abs)
import Effect (Effect)
import Effect.Class.Console (log)

small :: Int -> Int -> Int
small n x = abs x `mod` n

type TestMsg =
    { msg :: String
    , test :: Effect Unit
    }

runTest :: TestMsg -> Effect Unit
runTest { msg, test } = do
    log ""
    log $ "Running test: " <> msg
    test
    log ""
