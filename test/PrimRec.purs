module Test.PrimRec where

import Prelude (($),(<>), (==), (+), (*), Unit, discard)
import RecursiveModels.Parsing (anyTermP, definitionP, fromDefinitionP)
import RecursiveModels.PrimRec (Error(..), Term(..), TermWithRef(..), fromTerm, resolveName, unresolveTerm)
import Data.Either (Either(..))
import Data.List (List(..), (:), fromFoldable)
import Effect (Effect)
import Effect.Class.Console (log)
import Data.Tuple (Tuple(..))
import MetaModel (run)
import Parsing (runParser)
import RecursiveModels.Printing (printDef)
import Test.Assert (assertEqual)
import Test.QuickCheck (quickCheck)
import Test.Test (TestMsg, runTest, small)

testsPrimRec :: Effect Unit
testsPrimRec = do
    log "---------------------------------"
    log "Property tests"
    log "---------------------------------"
    runTest addPR
    runTest mulPR
    log "---------------------------------"
    log "Unit tests"
    log "---------------------------------"
    runTest resolve
    runTest resolveComplex
    runTest resolveCirc
    runTest resolveSelf
    runTest resolveMissing
    runTest resolveUnresolve
    runTest parseAdd
    runTest parseMul
    runTest parsePrint

--------------------------------------------------------------------------------
--# Property tests
--------------------------------------------------------------------------------

add :: Term
add = Rec
  (Proj 1)
  Succ

mul :: Term
mul = Rec
  (Const 0)
  (Comp add ((Proj 1):(Proj 3):Nil))

fact :: Term
fact = Rec
  (Const 1)
  (Comp mul ((Proj 1):(Proj 3):Nil))

addPR :: TestMsg
addPR =
    { test: quickCheck $
        \n m -> run add (small 100 n:small 100 m:Nil) == small 100 n + small 100 m
    , msg: "Addition"
    }

mulPR :: TestMsg
mulPR =
    { test: quickCheck $
        \n m -> run mul (small 15 n:small 15 m:Nil) == small 15 n * small 15 m
    , msg: "Multiplication"
    }

--------------------------------------------------------------------------------
--# Unit tests (parsing, resolving, unresolving)
--------------------------------------------------------------------------------

resolve :: TestMsg
resolve =
    { test: assertEqual
        { actual: resolveName defs "main"
        , expected: Right mul
        }
    , msg: "Resolve (success)"
    }
    where
        defs = fromFoldable
            [ Tuple "add" (Rec_ (Proj_ 1) Succ_)
            , Tuple "main" (Rec_ (Const_ 0)
                (Comp_ (Reference "add") ((Proj_ 1):(Proj_ 3):Nil)))
            ]

resolveComplex :: TestMsg
resolveComplex =
    { test: assertEqual
        { actual: resolveName defs "main"
        , expected: Right $ Rec
            (Const 0)
            (Comp
                (Rec
                    (Proj 1)
                        (Rec
                            (Proj 1)
                            (Const 11)
                        )
                )
                ((Proj 1):(Proj 3):Nil)
            )
        }
    , msg: "Resolve (forward reference)"
    }
    where
        defs = fromFoldable
            [ Tuple "main" (Rec_ (Const_ 0)
                (Comp_ (Reference "add") ((Proj_ 1):(Proj_ 3):Nil)))
            , Tuple "add" (Rec_ (Proj_ 1) (Reference "add2"))
            , Tuple "add2" (Rec_ (Proj_ 1) (Reference "add3"))
            , Tuple "add3" (Const_ 11)
            ]

resolveCirc :: TestMsg
resolveCirc =
    { test: assertEqual
        { actual: resolveName defs "main"
        , expected: Left CircularDependency
        }
    , msg: "Resolve (circular dependency)"
    }
    where
        defs = fromFoldable
            [ Tuple "add" (Rec_ (Proj_ 1) (Reference "main"))
            , Tuple "main" (Rec_ (Const_ 0)
                (Comp_ (Reference "add") ((Proj_ 1):(Proj_ 3):Nil)))
            ]

resolveSelf :: TestMsg
resolveSelf =
    { test: assertEqual
        { actual: resolveName defs "main"
        , expected: Left CircularDependency
        }
    , msg: "Resolve (circular dependency)"
    }
    where
        defs = fromFoldable
            [ Tuple "add" (Rec_ (Proj_ 1) (Reference "main"))
            , Tuple "main" (Rec_ (Const_ 0)
                (Comp_ (Reference "main") ((Proj_ 1):(Proj_ 3):Nil)))
            ]

resolveMissing :: TestMsg
resolveMissing =
    { test: assertEqual
        { actual: resolveName defs "main"
        , expected: Left $ MissingFunction "add"
        }
    , msg: "Resolve (missing definition)"
    }
    where
        defs = fromFoldable
            [ Tuple "main" (Rec_ (Const_ 0)
                (Comp_ (Reference "add") ((Proj_ 1):(Proj_ 3):Nil)))
            ]

resolveUnresolve :: TestMsg
resolveUnresolve =
    { test: assertEqual
        { actual: resolveName (unresolveTerm mul) "main"
        , expected: Right mul
        }
    , msg: "Resolve-unresolve roundtrip"
    }

parseAdd :: TestMsg
parseAdd =
    { test: assertEqual
        { actual: runParser "Recursion{ Base:P1 , Step:S}" anyTermP
        , expected: Right $ fromTerm add
        }
    , msg: "Parsing1"
    }

parseMul :: TestMsg
parseMul =
    { test: assertEqual
        { actual: runParser mulStr definitionP
        , expected: Right $ fromFoldable
            [ Tuple "add" (Rec_ (Proj_ 1) Succ_)
            , Tuple "main" (Rec_ (Const_ 0)
                (Comp_ (Reference "add") ((Proj_ 1):(Proj_ 3):Nil)))
            ]
        }
    , msg: "Parsing2"
    }
    where
        mulStr :: String
        mulStr =
               "add := Recursion { Base: P1, Step:S };\n"
            <> "main := Recursion {\n"
            <> "    Base: C0,\n"
            <> "    Step: add[P1, P3]\n"
            <> "}"

parsePrint :: TestMsg
parsePrint =
    { test: assertEqual
        { actual: runParser factStr (fromDefinitionP "main")
        , expected: Right $ fact
        }
    , msg: "Roundtrip (unresolve >> print Definition >> parse from Definition)"
    }
    where
        factStr :: String
        factStr =  printDef $ unresolveTerm fact
