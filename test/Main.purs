module Test.Main where

import Prelude
import Effect (Effect)
import Effect.Class.Console (log)
import Test.Auxiliary (testsAux)
import Test.FSM (testFSM)
import Test.Goto (testsGoto)
import Test.PrimRec (testsPrimRec)
import Test.Turing (testTuring)
import Test.WhileLoop (testsWhileLoop)

main :: Effect Unit
main = do
    log "======================================="
    log "Testing Auxiliary"
    log "======================================="
    testsAux
    log "======================================="
    log "Testing primitive recursive functions"
    log "======================================="
    testsPrimRec
    log "======================================="
    log "Testing Loop and While"
    log "======================================="
    testsWhileLoop
    log "======================================="
    log "Testing Goto"
    log "======================================="
    testsGoto
    log "======================================="
    log "Testing Turing Machine"
    log "======================================="
    testTuring
    log "======================================="
    log "Testing Finite State Machines"
    log "======================================="
    testFSM
    log "All tests passed!"

