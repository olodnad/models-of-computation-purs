module Test.FSM where

import Prelude

import Common.Auxiliary (strToCharList)
import Data.Char (toCharCode)
import Data.Either (Either(..), isRight)
import Data.Int (toStringAs, binary)
import Data.Map as M
import Data.Set as S
import Data.String (Pattern(..), contains)
import Data.String.CodeUnits (fromCharArray, toCharArray)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Class.Console (log)
import MachineModels.Common (Acceptance(..))
import MachineModels.FSM (DFSM(..), NFSM(..))
import MachineModels.Parsing (dfsmP, nfsmP)
import MachineModels.Printing (dfsmPrint, nfsmPrint)
import MetaModel (run)
import Parsing (parseErrorMessage, runParser)
import Test.Assert (assertEqual, assertTrue)
import Test.QuickCheck (quickCheck)
import Test.Test (TestMsg, runTest, small)

testFSM :: Effect Unit
testFSM =
    log "---------------------------------"
    *> log "Property tests"
    *> log "---------------------------------"
    *> runTest dfsmMod5T
    *> runTest nfsmAAT
    *> log "---------------------------------"
    *> log "Unit tests"
    *> log "---------------------------------"
    *> runTest parsePrintRoundtripDFSM
    *> runTest parsePrintRoundtripNFSM
    *> runTest parse

--------------------------------------------------------------------------------
--# Property tests
--------------------------------------------------------------------------------
dfsmMod5 :: DFSM Char String
dfsmMod5 = DFSM
    { initialState: "A0"
    , acceptingStates: S.singleton "A0"
    , transitions: M.fromFoldable
        [ (Tuple (Tuple "A0" '0') "A0")
        , (Tuple (Tuple "A0" '1') "A1")
        , (Tuple (Tuple "A1" '0') "A2")
        , (Tuple (Tuple "A1" '1') "A3")
        , (Tuple (Tuple "A2" '0') "A4")
        , (Tuple (Tuple "A2" '1') "A0")
        , (Tuple (Tuple "A3" '0') "A1")
        , (Tuple (Tuple "A3" '1') "A2")
        , (Tuple (Tuple "A4" '0') "A3")
        , (Tuple (Tuple "A4" '1') "A4")
        ]
    }

nfsmAA :: NFSM Char String
nfsmAA = NFSM
    { initialState: "A"
    , acceptingStates: S.singleton "C"
    , transitions: M.fromFoldable
        [ (Tuple (Tuple "A" 'a') (S.fromFoldable ["A", "B"]))
        , (Tuple (Tuple "A" 'b') (S.singleton "A"))
        , (Tuple (Tuple "B" 'a') (S.singleton "C"))
        , (Tuple (Tuple "C" 'a') (S.singleton "C"))
        , (Tuple (Tuple "C" 'b') (S.singleton "C"))
        ]
    }

dfsmMod5T :: TestMsg
dfsmMod5T =
    { test: quickCheck $
        \x -> case run dfsmMod5 (strToCharList (toBinary (small 1000 x))) of
            Accept _ -> small 1000 x `mod` 5 == 0
            _ -> small 1000 x `mod` 5 /= 0
    , msg: "Deterministic FSM"
    }
    where
        toBinary :: Int -> String
        toBinary = toStringAs binary

nfsmAAT :: TestMsg
nfsmAAT =
    { test: quickCheck $ \s -> case run nfsmAA (strToCharList (toAA s)) of
        Accept _ -> contains (Pattern "aa") (toAA s)
        _ -> not (contains (Pattern "aa") (toAA s))
    , msg: "Non deterministic FSM"
    }
    where
        f :: Int -> Char
        f x = if x `mod` 3 == 0 then 'a' else 'b'
        toAA :: String -> String
        toAA = toCharArray >>> (map (toCharCode >>> f)) >>> fromCharArray

--------------------------------------------------------------------------------
--# Unit tests (parsing)
--------------------------------------------------------------------------------

parsePrintRoundtripDFSM :: TestMsg
parsePrintRoundtripDFSM =
    { test: assertEqual
        { actual: case runParser mStr dfsmP of
            Left e -> Left $ parseErrorMessage e
            Right fsm -> Right (dfsmPrint fsm)
        , expected: Right mStr
        }
    , msg: "Parse-print roundtrip (DFSM)"
    }
    where
      mStr :: String
      mStr = "initial state: A0;\n"
        <> "accepting states: {A0};\n"
        <> "transitions:\n"
        <> "  (A0, 0) -> A0;\n"
        <> "  (A0, 1) -> A1;\n"
        <> "  (A1, 0) -> A2;\n"
        <> "  (A1, 1) -> A3;\n"
        <> "  (A2, 0) -> A4;\n"
        <> "  (A2, 1) -> A0;\n"
        <> "  (A3, 0) -> A1;\n"
        <> "  (A3, 1) -> A2;\n"
        <> "  (A4, 0) -> A3;\n"
        <> "  (A4, 1) -> A4"

parsePrintRoundtripNFSM :: TestMsg
parsePrintRoundtripNFSM =
    { test: assertEqual
        { actual: case runParser mStr nfsmP of
            Left e -> Left $ parseErrorMessage e
            Right fsm -> Right (nfsmPrint fsm)
        , expected: Right mStr
        }
    , msg: "Parse-print roundtrip (NFSM)"
    }
    where
      mStr :: String
      mStr = "initial state: A;\n"
        <> "accepting states: {C};\n"
        <> "transitions:\n"
        <> "  (A, a) -> {A, B};\n"
        <> "  (A, b) -> {A};\n"
        <> "  (B, a) -> {C};\n"
        <> "  (C, a) -> {C};\n"
        <> "  (C, b) -> {C}"

parse :: TestMsg
parse =
    { test: assertTrue $ isRight $ runParser mStr dfsmP
    , msg: "Admissible syntax styles"
    }
    where
      mStr :: String
      mStr = "initial state: A0;\n"
        <> "accepting states: {A0};\n"
        <> "transitions:\n"
        <> "  (A0, 0) -> A0;\n"
        <> "  (A0, 1) -> A1;\n"
        <> "(A1, 0) -> A2;\n"
        <> "(A1,1)->A3;\n"
        <> "  ( A2\n"
        <> "  , 0\n"
        <> "  ) -> A4;\n"
        <> "  (A2, 1) -> A0;\n"
        <> "  (A3, 0) -> A1;\n"
        <> "  (A3, 1) -> A2;\n"
        <> "  (A4, 0) -> A3;\n"
        <> "  (A4, 1) -> A4"