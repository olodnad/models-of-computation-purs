module Test.Goto where

import Prelude
import ProceduralModels.Common (Const(..), Variable(..))
import ProceduralModels.Goto (GTProgram(..), Instruction(..), Marker(..))
import Data.Either (Either(..), isLeft)
import Data.List (List(..), (:))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Class.Console (log)
import MetaModel (run, runBound)
import Parsing (runParser)
import ProceduralModels.Parsing (gotoProgramP)
import ProceduralModels.Printing (printGotoProgram)
import Test.Assert (assert, assertEqual)
import Test.QuickCheck (quickCheck)
import Test.Test (TestMsg, runTest, small)

testsGoto :: Effect Unit
testsGoto = do
    log "---------------------------------"
    log "Property tests"
    log "---------------------------------"
    runTest addGotoT
    runTest mulGotoT
    runTest foreverGotoT
    log "---------------------------------"
    log "Unit tests"
    log "---------------------------------"
    runTest parseAddGoto
    runTest parseFail
    runTest parsePrintRoundtrip

--------------------------------------------------------------------------------
--# Property tests
--------------------------------------------------------------------------------

addGoto :: GTProgram
addGoto = GTP $
    (Tuple (M 1) (IfGoto (X 1) (C 0) (M 5)))
    :(Tuple (M 2) (AssignMinusG (X 1) (X 1) (C 1)))
    :(Tuple (M 3) (AssignPlusG (X 2) (X 2) (C 1)))
    :(Tuple (M 4) (Goto (M 1)))
    :(Tuple (M 5) (AssignPlusG (X 0) (X 2) (C 0)))
    :(Tuple (M 6) Halt)
    :Nil

mulGoto :: GTProgram
mulGoto = GTP $
    ( Tuple (M 10) (IfGoto (X 1) (C 0) (M 40)))
    :( Tuple (M 20) (AssignMinusG (X 1) (X 1) (C 1)))
    :( Tuple (M 31) (AssignPlusG (X 3) (X 2) (C 0)))
    :( Tuple (M 32) (IfGoto (X 3) (C 0) (M 10)))
    :( Tuple (M 33) (AssignMinusG (X 3) (X 3) (C 1)))
    :( Tuple (M 34) (AssignPlusG (X 0) (X 0) (C 1)))
    :( Tuple (M 35) (Goto (M 32)))
    :( Tuple (M 40) Halt)
    :Nil

gotoForever :: GTProgram
gotoForever = GTP $
    ( Tuple (M 1) (Goto (M 2)))
    :( Tuple (M 2) (Goto (M 1)))
    :Nil

addGotoT :: TestMsg
addGotoT =
    { test: quickCheck $ \x y -> run addGoto (small 1000 x: small 1000 y:Nil)
        == small 1000 x + small 1000 y
    , msg: "Addition in Goto"
    }

mulGotoT :: TestMsg
mulGotoT =
    { test: quickCheck $ \x y -> run mulGoto (small 100 x: small 100 y:Nil)
        == small 100 x * small 100 y
    , msg: "Multiplication in Goto"
    }

foreverGotoT :: TestMsg
foreverGotoT =
    { test: quickCheck $ \n l -> isLeft (runBound gotoForever (small 1000 n)  l)
    , msg: "Forever Goto"
    }

--------------------------------------------------------------------------------
--# Unit tests (parsing)
--------------------------------------------------------------------------------

parseAddGoto :: TestMsg
parseAddGoto =
    { test: assertEqual
        { actual : runParser addStr gotoProgramP
        , expected : Right addGoto
        }
    , msg: "Parsing addition in Goto"
    }
    where
        addStr = "M1: if x1 = 0 then goto M5;\n\
                 \M2: x1 = x1 - 1;\n\
                 \M3: x2 = x2 + 1;\n\
                 \M4: goto M1;\n\
                 \M5: x0 = x2 + 0;\n\
                 \M6: halt"

parseFail :: TestMsg
parseFail =
    { test: assert $ isLeft (runParser faulty gotoProgramP)
    , msg: "Rejecting faulty Goto program"
    }
    where
        faulty = "M1: if x1 = 0 then goto M5;\n\
                 \M2: x1 = x1 - 1;\n\
                 \M3: x2 = x2 + 1\n\
                 \M4: goto M1;\n\
                 \M5: x0 = x2 + 0;\n\
                 \M6: halt"

parsePrintRoundtrip :: TestMsg
parsePrintRoundtrip =
    { test: assertEqual
        { actual : runParser (printGotoProgram mulGoto) gotoProgramP
        , expected : Right mulGoto
        }
    , msg: "Parsing and printing addition in Goto"
    }
