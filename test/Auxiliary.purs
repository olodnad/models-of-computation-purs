module Test.Auxiliary where

import Prelude
import Common.Auxiliary (abs, id, indexDef, lookup, mapTuple, prepReplicate, replicate)
import Data.List (List(..), length, singleton)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Test.QuickCheck (quickCheck)
import Test.Test (TestMsg, runTest)

testsAux :: Effect Unit
testsAux = do
    runTest absT
    runTest mapTupleT
    runTest lookupT
    runTest replicateLength
    runTest prepReplicateT
    pure unit

absT :: TestMsg
absT =
    { test: quickCheck $ \n ->
        abs n == abs (-n)
        && abs n >= 0
        && abs (abs n) == abs n
    , msg: "abs"
    }

mapTupleT :: TestMsg
mapTupleT =
    { test: quickCheck \(f :: Int -> Int) (g :: Int -> Int) (n :: Int) ->
        mapTuple (f <<< g) id (Tuple n (n+1))
        == mapTuple f id (mapTuple g id (Tuple n (n+1)))
    , msg: "mapTuple"
    }

lookupT :: TestMsg
lookupT =
    { test: quickCheck $ \(n :: Int) ->
        lookup n (singleton (Tuple n n)) == Just n
    , msg: "lookup"
    }

replicateLength :: TestMsg
replicateLength =
    { test: quickCheck $ \n ->
        length (replicate (n `mod` 20) 0) == n `mod` 20
    , msg: "Replicate length property"
    }

prepReplicateT :: TestMsg
prepReplicateT =
    { test: quickCheck f
    , msg: "prepReplicate and indexDef"
    }
    where
        f :: Int -> Boolean
        f n = indexDef 2 i xs == if i < 10 then 0 else if i < 20 then 1 else 2
            where
                xs = prepReplicate 10 0 (prepReplicate 10 1 Nil)
                i = n `mod` 25

{-

-}