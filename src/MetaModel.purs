module MetaModel where

import Prelude (($), (-))
import Data.Either (Either(..))
import Data.List (List(..), (:))
import Data.Tuple (Tuple(..), snd)

-------------------------------------------------------------------------------
--# Generic Computation Model
-------------------------------------------------------------------------------

class CompModel model input state result | model -> input state result where
    mInit :: model -> input -> state
    mUpdate :: model -> state -> Either state result

runTrace :: forall model input state output . CompModel model input state output
    => model -> input -> Tuple (List state) output
runTrace model input = runTrace_ Nil $ mInit model input
  where
      runTrace_ acc state = case mUpdate model state of
          Left state' -> runTrace_ (state:acc) state'
          Right result -> Tuple acc result

run :: forall model input state output . CompModel model input state output
    => model -> input -> output
run model input = snd $ runTrace model input

-- @dev: additional functions runBound and runTrace can be genrealized to
-- minimize code duplication.
runBound :: forall model input state output . CompModel model input state output
    => model -> Int -> input -> Either state output
runBound model bound input = runBound_ bound $ mInit model input
  where
    runBound_ 0 state = Left $ state
    runBound_ n state = case mUpdate model state of
        Left state' -> runBound_ (n - 1) state'
        Right result -> Right result

{--

    runTraceRevInteractive :: Monad m =>
      (MState model -> m a)
      -> model
      -> MInput model
      -> m ([MState model], MResult model)
    runTraceRevInteractive eff model input = runTraceRev_ [] $ mInit model input
      where
          runTraceRev_ acc state = do
              _ <- eff state
              case mUpdate model state of
                  Left state' -> runTraceRev_ (state':acc) state'
                  Right result -> return (acc, result)

-}