module Common.Auxiliary where

import Prelude
import Data.Array as Array
import Data.Either (Either(..), either)
import Data.Foldable (findMap, maximum)
import Data.List (List(..), (:), index)
import Data.List as List
import Data.Map as M
import Data.Maybe (Maybe(..))
import Data.String.CodeUnits (fromCharArray, toCharArray)
import Data.Tuple (Tuple(..))
import Data.String.CodeUnits as StrCu

-------------------------------------------------------------------------------
--# General Auxiliary Functions and Data Types
-------------------------------------------------------------------------------

id :: forall a . a -> a
id = identity

abs :: Int -> Int
abs x = if x < 0 then -x else x

mapTuple :: forall a b c d . (a -> c) -> (b -> d) -> Tuple a b -> Tuple c d
mapTuple f g (Tuple a b) = Tuple (f a) (g b)

data Triple a b c = Triple a b c

derive instance (Eq a, Eq b, Eq c) => Eq (Triple a b c)
derive instance (Ord a, Ord b, Ord c) => Ord (Triple a b c)

lookupDef :: forall k v. Ord k => v -> k -> M.Map k v -> v
lookupDef default k m = case M.lookup k m of
    Just v -> v
    Nothing -> default

mapRight :: forall l r s . (r -> s) -> Either l r -> Either l s
mapRight f = either Left (Right <<< f)

prepReplicate :: forall a . Int -> a -> List a -> List a
prepReplicate n a xs = case n of
    0 -> xs
    _ -> prepReplicate (n - 1) a (a:xs)

replicate :: forall a . Int -> a -> List a
replicate n a = prepReplicate n a Nil

maximumDef :: forall a . Ord a => a -> List a -> a
maximumDef a xs = case maximum xs of
    Just x -> max a x
    Nothing -> a

lookup :: forall a b. Eq a => a -> List (Tuple a b) -> Maybe b
lookup k0 = findMap (\(Tuple k v) -> if k==k0 then Just v else Nothing)

indexDef :: forall a . a -> Int -> List a -> a
indexDef def i xs = case index xs i of
    Just x -> x
    Nothing -> def

-------------------------------------------------------------------------------
--# String Manipulation and similar stuff
-------------------------------------------------------------------------------

charListToStr :: List Char -> String
charListToStr = fromCharArray <<< List.toUnfoldable

strToCharList :: String -> List Char
strToCharList = Array.toUnfoldable <<< toCharArray

char2Str :: Char -> String
char2Str c = StrCu.singleton c

strCons :: Char -> String -> String
strCons c s = char2Str c <> s

prepReplicateStr :: Int -> String -> String -> String
prepReplicateStr 0 _ w = w
prepReplicateStr n s w = prepReplicateStr (n - 1) s (s<>w)