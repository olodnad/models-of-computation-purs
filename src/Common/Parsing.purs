module Common.Parsing where

import Prelude
import Common.Auxiliary (Triple(..), charListToStr)
import Data.List (List, (:))
import Data.Set as S
import Parsing (Parser)
import Data.Tuple (Tuple(..))
import Parsing.Combinators (many, sepBy)
import Parsing.String (string, anyChar)
import Parsing.String.Basic (alphaNum, letter, lower, skipSpaces)

type Parser_ a = Parser String a

lexeme :: forall a. Parser_ a -> Parser_ a
lexeme p = p <* skipSpaces

skipStrS :: String -> Parser_ Unit
skipStrS w = string w *> pure unit

skipStrW :: String -> Parser_ Unit
skipStrW w = lexeme $ skipStrS w

namePGeneric :: Parser_ Char -> Parser_ String
namePGeneric head = lexeme $ cons <$> head <*> many alphaNum
    where
        cons :: Char -> List Char -> String
        cons c cs = charListToStr $ c:cs

nameP :: Parser_ String
nameP = namePGeneric letter

lowerNameP :: Parser_ String
lowerNameP = namePGeneric lower

symbolP :: Parser_ Char
symbolP = lexeme $ anyChar

setP :: forall a. Ord a => Parser_ a -> Parser_ (S.Set a)
setP p = S.fromFoldable <$>
    lexeme (skipStrW "{" *> (sepBy p (skipStrW ",")) <* skipStrW "}")

pairP :: forall a b. Parser_ a -> Parser_ b -> Parser_ (Tuple a b)
pairP pa pb = Tuple
    <$> (skipStrW "(" *> pa)
    <*> (skipStrW "," *> pb <* skipStrW ")")

tripleP :: forall a b c.
    Parser_ a
    -> Parser_ b
    -> Parser_ c
    -> Parser_ (Triple a b c)
tripleP pa pb pc = Triple
    <$> (skipStrW "(" *> pa)
    <*> (skipStrW "," *> pb)
    <*> (skipStrW "," *> pc <* skipStrW ")")

lsemi :: Parser_ Unit
lsemi = skipStrW ";"

listLikeP :: forall a.
    Parser_ Unit ->
    Parser_ Unit ->
    Parser_ Unit ->
    Parser_ a ->
    Parser_ (List a)
listLikeP open close delim p = do
    open
    xs <- p `sepBy` delim
    close
    pure xs
