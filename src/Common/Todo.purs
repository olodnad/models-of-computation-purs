module Common.Todo where

import Effect.Exception.Unsafe (unsafeThrow)

todo :: forall a. a
todo =  unsafeThrow "TODO"