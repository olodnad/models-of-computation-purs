module ProceduralModels.LoopWhile where

import Prelude
import ProceduralModels.Common (Const(..), ProcState(..), Variable(..), initPState, readValue, reassignMinus, reassignPlus)
import Common.Auxiliary (mapTuple, id, prepReplicate)
import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)
import Data.List (List, singleton,(:))
import Data.Either (Either(..))
import Data.Tuple (Tuple(..), fst, snd)
import MetaModel (class CompModel)

data LWProgram
    = AssignPlus Variable Variable Const
    | AssignMinus Variable Variable Const
    | Loop Variable LWProgram
    | While Variable LWProgram
    | Seq LWProgram LWProgram

instance CompModel
    LWProgram
    (List Int)
    (ProcState (List LWProgram))
    Int
    where
        mInit prog = initPState (singleton prog)

        mUpdate _ (ProcState state) = case state.innerState of
            (AssignMinus (X i) (X j) (C c)) : ps -> Left $ ProcState
                { innerState : ps
                , valueStore : reassignMinus (X i) (X j) (C c) state.valueStore
                }
            (AssignPlus (X i) (X j) (C c)) : ps -> Left $ ProcState
                { innerState : ps
                , valueStore : reassignPlus (X i) (X j) (C c) state.valueStore
                }
            (Seq p q) : ps -> Left $ ProcState
                { innerState : p:q:ps
                , valueStore : state.valueStore
                }
            (Loop (X i) q) : ps -> Left $ ProcState
                { innerState : prepReplicate (readValue i state.valueStore) q ps
                , valueStore : state.valueStore
                }
            (While (X i) p) : ps -> Left $ ProcState
                { innerState : if readValue i state.valueStore == 0
                    then ps
                    else p:state.innerState
                , valueStore : state.valueStore
                }
            _empty -> Right $ readValue 0 state.valueStore

-- | With the LWProgram type it is possible to define Loop and While programs
--   as well as programs that are a mix of both (containing Loop and While).
--   The type "Loop", "While", "Any" or "Mixed" of the program is determined by
--   the getProgramType function.
data ProgramType
    = LoopType
    | WhileType
    | MixedType
    | AnyType

getProgramType :: LWProgram -> ProgramType
getProgramType prog = case count prog of
    Tuple true false  -> LoopType
    Tuple false true  -> WhileType
    Tuple true true   -> MixedType
    Tuple false false -> AnyType
    where
        count :: LWProgram -> Tuple Boolean Boolean
        count p = case p of
            Loop _ p' -> mapTuple (const true) id (count p')
            While _ p' -> mapTuple id (const true) (count p')
            Seq p1 p2 -> mapTuple
                ((||) (fst (count p1)))
                ((||) (snd (count p1)))
                (count p2)
            _ -> Tuple false false

--------------------------------------------------------------------------------
--# Eq, Ord, Show instances
--------------------------------------------------------------------------------

derive instance Eq LWProgram
derive instance Ord LWProgram
derive instance Generic LWProgram _
instance Show LWProgram where
    show x = genericShow x