module ProceduralModels.Goto where

import Prelude
import MetaModel (class CompModel, mUpdate)
import ProceduralModels.Common (Const(..), ProcState(..), Variable(..), initPState, readValue, reassignMinus, reassignPlus)
import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)
import Data.List (List(..), (:))
import Data.Tuple (Tuple(..))
import Data.Either (Either(..))

newtype Marker = M Int

data Instruction
    = AssignPlusG Variable Variable Const
    | AssignMinusG Variable Variable Const
    | IfGoto Variable Const Marker
    | Goto Marker
    | Halt

type Line = Tuple Marker Instruction

newtype GTProgram = GTP (List Line)

instance CompModel
    GTProgram
    (List Int)
    (ProcState (List Line))
    Int
    where
        mInit (GTP ls) = initPState ls
        mUpdate p0 (ProcState st) = case st.innerState of
            Nil -> Right $ readValue 0 st.valueStore
            (Tuple _ (AssignPlusG (X i) (X j) (C c))):lines -> Left $ ProcState
                { innerState : lines
                , valueStore : reassignPlus (X i) (X j) (C c) st.valueStore
                }
            (Tuple _ (AssignMinusG (X i) (X j) (C c))):lines -> Left $ ProcState
                { innerState : lines
                , valueStore : reassignMinus (X i) (X j) (C c) st.valueStore
                }
            (Tuple _ (Goto m)):_ -> Left $ inner $ gotoL m p0
            (Tuple _ (IfGoto (X i) (C c) m)):lines -> Left $ inner
                ( if readValue i st.valueStore == c
                    then gotoL m p0
                    else lines
                )
            (Tuple _ Halt):_ -> mUpdate p0 $ inner Nil
          where
            inner is = ProcState $ st{innerState = is}
            gotoL :: Marker -> GTProgram -> List Line
            gotoL m (GTP lines) = case lines of
                Nil -> Nil
                (Tuple m' _):lines_
                    | m' == m -> lines
                    | otherwise -> gotoL m (GTP lines_)

--------------------------------------------------------------------------------
--# Eq, Ord, Show instances
--------------------------------------------------------------------------------

derive instance Eq Instruction
derive instance Ord Instruction
derive instance Generic Instruction _
instance Show Instruction where
    show x = genericShow x

derive instance Eq Marker
derive instance Ord Marker
derive instance Generic Marker _
instance Show Marker where
    show x = genericShow x


derive instance Eq GTProgram
derive instance Ord GTProgram
derive instance Generic GTProgram _
instance Show GTProgram where
    show x = genericShow x
