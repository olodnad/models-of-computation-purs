module ProceduralModels.Parsing where

import Prelude
import Common.Parsing (Parser_, lexeme, lsemi, skipStrS, skipStrW)
import Control.Lazy (defer)
import Data.List (List(..), (:))
import Data.List.NonEmpty (NonEmptyList(..))
import Data.NonEmpty (NonEmpty, (:|))
import Parsing.Combinators (choice, sepBy, sepBy1, try)
import Parsing.String (eof)
import Data.Tuple (Tuple(..))
import Parsing.String.Basic (intDecimal)
import ProceduralModels.Common (Const(..), Variable(..))
import ProceduralModels.Goto (GTProgram(..), Instruction(..), Line, Marker(..))
import ProceduralModels.LoopWhile (LWProgram(..))

variableP :: Parser_ Variable
variableP = X <$> (skipStrS "x" *> lexeme intDecimal)

constP :: Parser_ Const
constP = C <$> lexeme intDecimal

assignGenP :: forall a.
    (Variable -> Variable -> Const -> a)
    -> String
    -> Parser_ a
assignGenP constructor ops = constructor
    <$> (variableP <* skipStrW "=")
    <*> variableP
    <*> (skipStrW ops *> constP)

--------------------------------------------------------------------------------
--# Goto Programs
--------------------------------------------------------------------------------

assignPlusGP :: Parser_ Instruction
assignPlusGP = assignGenP AssignPlusG "+"

assignMinusGP :: Parser_ Instruction
assignMinusGP = assignGenP AssignMinusG "-"

markerP :: Parser_ Marker
markerP = M <$> (skipStrS "M" *> lexeme intDecimal)

gotoP :: Parser_ Instruction
gotoP = Goto <$> (skipStrW "goto" *> markerP)

ifGotoP :: Parser_ Instruction
ifGotoP = IfGoto
    <$> (skipStrW "if" *> variableP)
    <*> (skipStrW "=" *> constP <* skipStrW "then goto")
    <*> markerP

haltP :: Parser_ Instruction
haltP = Halt <$ skipStrW "halt"

lineP :: Parser_ Line
lineP = Tuple
    <$> (markerP <* skipStrW ":")
    <*> (choice (map try [assignPlusGP, assignMinusGP, gotoP, ifGotoP, haltP]))

gotoProgramP :: Parser_ GTProgram
gotoProgramP = GTP <$> (lineP `sepBy` lsemi) <* eof

--------------------------------------------------------------------------------
--# While and Loop Programs
--------------------------------------------------------------------------------

assignPlusLW :: Parser_ LWProgram
assignPlusLW = assignGenP AssignPlus "+"

assignMinusLW :: Parser_ LWProgram
assignMinusLW = assignGenP AssignMinus "-"

loopP :: Parser_ LWProgram
loopP = Loop <$>
    (skipStrW "loop" *> variableP <* skipStrW "do")
    <*> ((defer \_ -> lwProgramPSeq) <* skipStrW "end")

whileP :: Parser_ LWProgram
whileP = While <$>
    (skipStrW "while" *> variableP
        <* skipStrW ">"
        <* skipStrW "0"
        <* skipStrW "do"
    )
    <*> ((defer \_ -> lwProgramPSeq) <* skipStrW "end")

lwProgramP_ :: Parser_ LWProgram
lwProgramP_ = choice $ map try
    [ assignPlusLW
    , assignMinusLW
    , defer \_ -> loopP
    , defer \_ -> whileP
    ]

lwProgramPSeq :: Parser_ LWProgram
lwProgramPSeq = (seq_ <<< unpack) <$> lwProgramP_ `sepBy1` lsemi
  where
    -- seems disgusting, but I don't know how to get pattern matching to work
    -- directly with NonEmptyList.
    unpack :: forall a. NonEmptyList a -> (NonEmpty List) a
    unpack (NonEmptyList xs) = xs
    seq_ :: (NonEmpty List) LWProgram -> LWProgram
    seq_ (p:|Nil) = p
    seq_ (p:|(q:ps)) = Seq p (seq_ (q:|ps))

lwProgramP :: Parser_ LWProgram
lwProgramP = lwProgramPSeq <* eof
