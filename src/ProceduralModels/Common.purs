module ProceduralModels.Common where

import Prelude
import Common.Auxiliary (lookupDef)
import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)
import Data.List (List, zip, range, length)
import Data.Map as M

--------------------------------------------------------------------------------
--# Generic Types and Functions for Procedural Models
--------------------------------------------------------------------------------

newtype Const = C Int
newtype Variable = X Int
type ValueStore = M.Map Int Int

readValue :: Int -> ValueStore -> Int
readValue = lookupDef 0

writeValue :: Int -> Int -> ValueStore -> ValueStore
writeValue = M.insert

-- xi <- op xj c
reassignOp ::
    (Int -> Int -> Int)
    -> Variable
    -> Variable
    -> Const
    -> ValueStore
    -> ValueStore
reassignOp op (X i) (X j) (C c) store =
    writeValue i (op (readValue j store) c) store

reassignPlus :: Variable -> Variable -> Const -> ValueStore -> ValueStore
reassignPlus = reassignOp (+)

reassignMinus :: Variable -> Variable -> Const -> ValueStore -> ValueStore
reassignMinus = reassignOp (\x y -> max 0 (x - y))

-- | All procedural models have a value store as part of their state.
data ProcState state = ProcState
    { innerState :: state
    , valueStore :: ValueStore
    }

-- | Generic way to preload input into the first n variables.
initPState :: forall state . state -> List Int -> ProcState state
initPState state input = ProcState
    { innerState : state
    , valueStore : M.fromFoldable $ zip (range 1 (length input)) input
    }

--------------------------------------------------------------------------------
--# Eq, Ord, Show instances
--------------------------------------------------------------------------------

derive instance Eq Const
derive instance Ord Const
derive instance Generic Const _
instance Show Const where
    show x = genericShow x

derive instance Eq Variable
derive instance Ord Variable
derive instance Generic Variable _
instance Show Variable where
    show x = genericShow x