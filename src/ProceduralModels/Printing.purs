module ProceduralModels.Printing where

import Prelude
import ProceduralModels.Common (Const(..), Variable(..))
import Data.Tuple (Tuple(..))
import ProceduralModels.Goto (GTProgram(..), Line, Marker(..), Instruction(..))
import Common.Auxiliary (prepReplicateStr)
import Data.Foldable (intercalate)
import ProceduralModels.LoopWhile (LWProgram(..))

printVariable :: Variable -> String
printVariable (X i) = "x" <> show i

printConst :: Const -> String
printConst (C c) = show c

printAssignGen :: String ->Variable -> Variable -> Const -> String
printAssignGen op xi xj c =
    printVariable xi
    <> " = "
    <> printVariable xj
    <> " " <> op <> " "
    <> printConst c

--------------------------------------------------------------------------------
--# Goto Programs
--------------------------------------------------------------------------------

printMarker :: Marker -> String
printMarker (M i) = "M" <> show i

printLine :: Line -> String
printLine (Tuple m i) = case i of
    (AssignPlusG xi xj c) -> fm $ printAssignGen "+" xi xj c
    (AssignMinusG xi xj c) -> fm $ printAssignGen "-" xi xj c
    IfGoto xi c m' -> fm $
        "if " <> printVariable xi <> " = " <> printConst c
        <> " then goto " <> printMarker m'
    Goto m' -> fm $ "goto " <> printMarker m'
    Halt -> fm $ "halt"
    where
        fm = (<>) (printMarker m <> ": ")

printGotoProgram :: GTProgram -> String
printGotoProgram (GTP ls) = intercalate ";\n" $ map printLine ls

--------------------------------------------------------------------------------
--# While and Loop Programs
--------------------------------------------------------------------------------

printWhileLoopProgram :: LWProgram -> String
printWhileLoopProgram p = printOff 0 p
    where
        printOff :: Int -> LWProgram -> String
        printOff off p = case p of
            AssignPlus xi xj c -> offset $ printAssignGen "+" xi xj c
            AssignMinus xi xj c -> offset $ printAssignGen "-" xi xj c
            Loop xi p' ->
                offset "loop " <> printVariable xi <> " do\n"
                <> printOff (off + 2) p' <> "\n"
                <> offset "end"
            While xi p' ->
                offset "while " <> printVariable xi <> ">0 do\n"
                <> printOff (off + 2) p' <> "\n"
                <> offset "end"
            Seq p1 p2 ->
                printOff off p1 <> ";\n"
                <> printOff off p2
            where
                offset :: String -> String
                offset = prepReplicateStr off " "