module RecursiveModels.PrimRec where

import Prelude
import Common.Auxiliary (indexDef, lookup, maximumDef)
import Data.Either (Either(..))
import Data.Generic.Rep (class Generic)
import Data.List (List(..), (:), range, length, zipWith, singleton, nub, reverse)
import Data.Maybe (Maybe(..))
import Data.Show.Generic (genericShow)
import Data.Traversable (traverse)
import Data.Tuple (Tuple(..), fst)
import MetaModel (class CompModel)

-------------------------------------------------------------------------------
--# Core Datatype
-------------------------------------------------------------------------------
-- | Terms describe primitive recursive functions
data Term
    = Const Int
    | Proj Int
    | Succ
    | Comp Term (List Term)
    | Rec Term Term

data PRApplication
    = Done Int
    | Ongoing Term (List PRApplication)

-- @dev: This is the core of evaluating primitive recursive functions and this
--       is also where we have to start to improve the efficiency of the model.
unfold :: PRApplication -> PRApplication
unfold (Ongoing t as) = case t of
    Const x -> Done x
    Proj i -> indexDef (Done 0) (i-1) as
    Succ -> case as of
        Nil -> Done 1
        (Done x):_ -> Done (x+1)
        -- A tiny bit of lazy evaluation by only evaluating the first argument
        -- and forgetting about the rest.
        ongoing:_  -> Ongoing Succ (singleton (unfold ongoing))
    Comp g fs -> Ongoing g $ map (\f -> Ongoing f as) fs
    Rec c g -> case as of
        Nil -> Ongoing c Nil
        (Done n):as'
            | n <= 0 -> Ongoing c as'
            | otherwise ->
                Ongoing g $ Ongoing (Rec c g) (Done (n-1):as'):Done n:as'
        ongoing:as' -> Ongoing t $ unfold ongoing:as'
unfold done = done

instance CompModel Term (List Int) PRApplication Int where
    mInit term xs = Ongoing term $ Done <$> xs
    mUpdate _ app = case unfold app of
        Done x -> Right x
        ongoing -> Left ongoing

-------------------------------------------------------------------------------
--# Code to Improve the Ergonomics of Primitive Recursive Functions
-------------------------------------------------------------------------------
type Name = String

-- | A term, possibly with references to other terms.
data TermWithRef
    = Reference Name
    | Const_ Int
    | Proj_ Int
    | Succ_
    | Comp_ TermWithRef (List TermWithRef)
    | Rec_ TermWithRef TermWithRef

fromTerm :: Term -> TermWithRef
fromTerm t = case t of
    Const x -> Const_ x
    Proj i -> Proj_ i
    Succ -> Succ_
    Comp g fs -> Comp_ (fromTerm g) (map fromTerm fs)
    Rec c g -> Rec_ (fromTerm c) (fromTerm g)

-- | A Definition consisting of multiple terms referring each other.
type MultiDef = List (Tuple Name TermWithRef)

data Error
    = MissingFunction Name
    | CircularDependency

resolveName :: MultiDef -> Name -> Either Error Term
resolveName multiDef name = resolveTerm 0 multiDef (Reference name)
    where
        resolveTerm :: Int -> MultiDef -> TermWithRef -> Either Error Term
        resolveTerm i md t
            | i > length md = Left CircularDependency
            | otherwise = case t of
                Const_ c -> Right $ Const c
                Proj_ j -> Right $ Proj j
                Succ_ -> Right Succ
                Reference n -> case lookup n md of
                    Just t' -> resolveTerm (i+1) md t'
                    Nothing -> Left $ MissingFunction n
                Comp_ g fs -> Comp
                    <$> resolveTerm i md g
                    <*> traverse (resolveTerm i md) fs
                Rec_ c g -> Rec
                    <$> resolveTerm i md c
                    <*> resolveTerm i md g

-- | Unresolving is the relatively complicated process of transforming a Term
--   into a MultiDef. The idea is that a MultiDef is a more human readable way
--   to display Terms.
unresolveTerm :: Term -> MultiDef
unresolveTerm term =
    (singleton >>> unresolve >>> nub >>> reverse >>> rename) term
    where
        unresolve :: List Term -> MultiDef
        unresolve Nil = Nil
        unresolve (t:ts)
            | depth t <= 1 = (Tuple (codeT t) (fromTerm t)):unresolve ts
            | otherwise = case t of
                Comp g fs ->
                    ( Tuple
                        (codeT t)
                        (Comp_ (Reference (codeT g)) (map (Reference <<< codeT) fs))
                    ):unresolve (g:fs <> ts)
                Rec c g ->
                    ( Tuple
                        (codeT t)
                        (Rec_ (Reference (codeT c)) (Reference (codeT g)))
                    ):unresolve (c:g:ts)
                _ -> (Tuple (codeT t) (fromTerm t)):unresolve ts

        depth :: Term -> Int
        depth t = case t of
            Comp g fs -> 1 + maximumDef 0 (map depth (g:fs))
            Rec c g -> 1 + max (depth c) (depth g)
            _ -> 0

        codeT :: Term -> String
        codeT t = show $ fromTerm t

        subNamesInside :: List (Tuple Name Name) -> TermWithRef -> TermWithRef
        subNamesInside m t = case t of
            Reference n -> case lookup n m of
                Just n' -> Reference n'
                Nothing -> Reference n
            Comp_ g fs  -> Comp_ (subNamesInside m g) (map (subNamesInside m) fs)
            Rec_ c g    -> Rec_ (subNamesInside m c) (subNamesInside m g)
            _           -> t

        rename :: MultiDef -> MultiDef
        rename m = map f m
            where
                originalNames :: List Name
                originalNames = map (fst) m
                newNames :: List Name
                newNames = map (((<>) "f") <<< show) (range 1 (length m))

                renamingSchema :: Name -> Name -> Tuple Name Name
                renamingSchema n1 n2
                    | n1 == codeT term = Tuple n1 "main"
                    | otherwise = Tuple n1 n2

                substitutions :: List (Tuple Name Name)
                substitutions = zipWith renamingSchema originalNames newNames

                f (Tuple n t) = case lookup n substitutions of
                    Just n' -> Tuple n' (subNamesInside substitutions t)
                    Nothing -> Tuple n (subNamesInside substitutions t)

--------------------------------------------------------------------------------
--# Eq, Ord, Show instances
--------------------------------------------------------------------------------

derive instance Eq TermWithRef
derive instance Ord TermWithRef
derive instance Generic TermWithRef _
instance Show TermWithRef where
    show x = genericShow x

derive instance Eq Term
instance Show Term where
    show = fromTerm >>> show

derive instance Eq Error
derive instance Generic Error _
instance Show Error where
    show x = genericShow x