module RecursiveModels.Parsing where

import Prelude hiding (between)
import Common.Parsing (Parser_, lexeme, lowerNameP, lsemi, skipStrS, skipStrW, listLikeP)
import Control.Lazy (defer)
import Data.Either (Either(..))
import Parsing (fail)
import Data.Tuple (Tuple(..))
import Parsing.Combinators (between, choice, sepBy, try, (<|>))
import Parsing.String (eof)
import Parsing.String.Basic (intDecimal)
import RecursiveModels.PrimRec (Error(..), MultiDef, Term, TermWithRef(..), resolveName)

refP :: Parser_ TermWithRef
refP = Reference <$> lowerNameP

constP :: Parser_ TermWithRef
constP = Const_ <$> lexeme (skipStrS "C" *> intDecimal)

projP :: Parser_ TermWithRef
projP = Proj_ <$> lexeme (skipStrS "P" *> intDecimal)

succP :: Parser_ TermWithRef
succP = Succ_ <$ skipStrW "S"

simpleTermP :: Parser_ TermWithRef
simpleTermP = choice [refP, constP, projP, succP]

recTermP :: Parser_ TermWithRef
recTermP = do
    skipStrW "Recursion"
    between (skipStrW "{") (skipStrW "}") $ do
        skipStrW "Base:"
        c <- anyTermP
        skipStrW ","
        skipStrW "Step:"
        g <- anyTermP
        pure $ Rec_ c g

compTermP :: Parser_ TermWithRef
compTermP = do
    head <- simpleTermP
        <|> between (skipStrW "(") (skipStrW ")") (defer \_ -> anyTermP)
    args <- listLikeP (skipStrW "[") (skipStrW "]") (skipStrW ",") anyTermP
    pure $ Comp_ head args

anyTermP :: Parser_ TermWithRef
anyTermP = defer \_ -> choice [try recTermP, try compTermP, try simpleTermP]

namedTermP :: Parser_ (Tuple String TermWithRef)
namedTermP = Tuple <$> (lexeme lowerNameP) <*> (skipStrW ":=" *> anyTermP)

definitionP :: Parser_ MultiDef
definitionP = (namedTermP `sepBy` lsemi) <* eof

fromDefinitionP :: String -> Parser_ Term
fromDefinitionP name = do
    defs <- definitionP
    case resolveName defs name of
        Left (MissingFunction fn) -> fail $
            "Missing function \"" <> fn <> "\" in definition"
        Left CircularDependency -> fail $ "Circular dependency in definition"
        Right t -> pure t
