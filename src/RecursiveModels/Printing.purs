module RecursiveModels.Printing where

import Prelude hiding (between)
import Common.Auxiliary (prepReplicateStr)
import Data.Foldable (intercalate)
import Data.Tuple (Tuple(..))
import RecursiveModels.PrimRec (MultiDef, TermWithRef(..))

-- | Printing a TermWithRef.
-- @dev: assumes that no nested recursion is present in the term. Use the
-- unresolve function to "pread out" nested recursion.
printFlatTerm :: TermWithRef -> String
printFlatTerm t = case t of
    Reference name -> name
    Const_ x -> "C" <> show x
    Proj_ i -> "P" <> show i
    Succ_ -> "S"
    Comp_ g fs ->
        printHead g <> "["
        <> intercalate ", " (map printFlatTerm fs)
        <> "]"
    Rec_ c g ->
        "Recursion {\n"
        <> offset 2 "Base: "
        <> printFlatTerm c
        <> ",\n"
        <> offset 2 "Step: "
        <> printFlatTerm g
        <> "\n"
        <> "}"
    where
        offset n = prepReplicateStr n " "
        printHead :: TermWithRef -> String
        printHead tr@(Comp_ _ _) = "(" <> printFlatTerm tr <> ")"
        printHead tr@(Rec_ _ _) = "(" <> printFlatTerm tr <> ")"
        printHead tr = printFlatTerm tr

printDef :: MultiDef -> String
printDef df = intercalate ";\n" $ map printDefLine df
    where
        printDefLine (Tuple n t) = n <> " := " <> printFlatTerm t
