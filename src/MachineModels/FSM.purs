module MachineModels.FSM where

import Prelude
import Common.Auxiliary (lookupDef)
import MachineModels.Common (Acceptance(..))
import Data.Either (Either(..))
import Data.List (List, (:))
import Data.Map as M
import Data.Maybe (Maybe(..))
import Data.Set as S
import Data.Tuple (Tuple(..))
import MetaModel (class CompModel)

-------------------------------------------------------------------------------
--# Deterministic Finite State Machines
-------------------------------------------------------------------------------

-- | A configuration of a finite state machine. In case of a deterministic
-- machines the state parameter is a single state, in case of non-deterministic
-- machines the state parameter is a set of states.
newtype ConfigFSM sigma state = ConfigFSM
    { currentState :: state
    , remainingInput :: List sigma
    }

-- | Datatype representing a deterministic finite state machine.
newtype DFSM sigma state = DFSM
    { initialState :: state
    , acceptingStates :: S.Set state
    , transitions :: M.Map (Tuple state sigma) state
    }

instance (Ord state, Ord sigma) => CompModel
    (DFSM sigma state)
    (List sigma)
    (ConfigFSM sigma state)
    (Acceptance sigma state)
    where
        mInit (DFSM { initialState : is}) input =
            ConfigFSM { currentState : is, remainingInput : input}

        mUpdate (DFSM {transitions: m}) (ConfigFSM {currentState: s, remainingInput: (a:w)}) =
            case M.lookup (Tuple s a) m of
                Just s' -> Left $ ConfigFSM {currentState: s', remainingInput: w}
                Nothing -> Right $ Terminated (a:w) s
        mUpdate (DFSM {acceptingStates: acc}) (ConfigFSM {currentState: s})
            | s `S.member` acc = Right $ Accept s
            | otherwise = Right $ Reject s

-------------------------------------------------------------------------------
--# Non-Deterministic Finite State Machines
-------------------------------------------------------------------------------

-- | Datatype representing a non-deterministic finite state machine.
newtype NFSM sigma state = NFSM
    { initialState :: state
    , acceptingStates :: S.Set state
    , transitions :: M.Map (Tuple state sigma) (S.Set state)
    }

instance (Ord state, Ord sigma) => CompModel
    (NFSM sigma state)
    (List sigma)
    (ConfigFSM sigma (S.Set state))
    (Acceptance sigma (S.Set state))
    where
        mInit (NFSM { initialState : is}) input = ConfigFSM
            { currentState : S.singleton is
            , remainingInput : input
            }
        mUpdate (NFSM {transitions: m}) (ConfigFSM {currentState: css, remainingInput: (a:w)})
            | S.isEmpty css = Right $ Terminated (a:w) css
            | otherwise = Left $ ConfigFSM {currentState: css', remainingInput: w}
            where
                collectStates :: (state -> S.Set state) -> S.Set state -> S.Set state
                collectStates f = S.unions <<< S.map f
                css' = collectStates (\s -> lookupDef S.empty (Tuple s a) m) css
        mUpdate (NFSM {acceptingStates: acc}) (ConfigFSM {currentState: cs})
            | S.isEmpty (cs `S.intersection` acc)  = Right $ Reject cs
            | otherwise = Right $ Accept $ cs `S.intersection` acc

