module MachineModels.Parsing where

import Prelude
import Common.Parsing (Parser_, nameP, symbolP, pairP, tripleP, setP, lsemi, skipStrW)
import Data.Map as M
import Data.Set as S
import Data.Tuple (Tuple(..))
import MachineModels.FSM (DFSM(..), NFSM(..))
import MachineModels.TuringM (TuringMachineAcceptor(..), TuringMachineCalc(..), Direction(..))
import Parsing.Combinators (sepBy, choice)
import Parsing.String (eof)

initialStateP :: Parser_ String
initialStateP = skipStrW "initial state:" *> nameP

acceptingStatesP :: Parser_ (S.Set String)
acceptingStatesP =
    skipStrW "accepting states:"
    *> setP nameP

-- | Parse a single transition
transitionP :: forall head tail.
    Parser_ head
    -> Parser_ tail
    -> Parser_ (Tuple head tail)
transitionP headP tailP = Tuple
    <$> (headP <* skipStrW "->")
    <*> tailP

-- | Parse many transitions as a map
transitionsP :: forall head tail.
    Ord head
    => Parser_ head
    -> Parser_ tail
    -> Parser_ (M.Map head tail)
transitionsP headP tailP = M.fromFoldable
    <$> (skipStrW "transitions:" *> (transitionP headP tailP) `sepBy` lsemi)

--------------------------------------------------------------------------------
--# Finite State Machines
--------------------------------------------------------------------------------

-- | Parse a deterministic finite state machine
dfsmP :: Parser_ (DFSM Char String)
dfsmP = do
    is <- initialStateP
    lsemi
    as <- acceptingStatesP
    lsemi
    ts <- transitionsP (pairP nameP symbolP) nameP
    _ <- eof
    pure $ DFSM
        { initialState: is
        , acceptingStates: as
        , transitions: ts
        }

-- | Parse a non-deterministic finite state machine
nfsmP :: Parser_ (NFSM Char String)
nfsmP = do
    is <- initialStateP
    lsemi
    as <- acceptingStatesP
    lsemi
    ts <- transitionsP (pairP nameP symbolP) (setP nameP)
    _ <- eof
    pure $ NFSM
        { initialState: is
        , acceptingStates: as
        , transitions: ts
        }

--------------------------------------------------------------------------------
--# Turing Machines
--------------------------------------------------------------------------------

directionP :: Parser_ Direction
directionP = choice
    [ L <$ skipStrW "L"
    , R <$ skipStrW "R"
    , N <$ skipStrW "N"
    ]

blankP :: Parser_ Char
blankP = skipStrW "blank:"
    *> skipStrW "'"
    *> symbolP
    <* skipStrW "'"

-- | Parse a Turing machine (I/O device)
tmCalcP :: Parser_ (TuringMachineCalc Char String)
tmCalcP = do
    is <- initialStateP
    lsemi
    b <- blankP
    lsemi
    ts <- transitionsP (pairP nameP symbolP) (tripleP nameP symbolP directionP)
    _ <- eof
    pure $ TMC $
        { initialState: is
        , blank: b
        , transitions: ts
        }

tmAccP :: Parser_ (TuringMachineAcceptor Char String)
tmAccP = do
    is <- initialStateP
    lsemi
    as <- acceptingStatesP
    lsemi
    b <- blankP
    lsemi
    ts <- transitionsP (pairP nameP symbolP) (tripleP nameP symbolP directionP)
    _ <- eof
    pure $ TMA
        { baseTm:
            { initialState: is
            , blank: b
            , transitions: ts
            }
        , acceptingStates: as
        }
