module MachineModels.Common where

import Data.List (List)

-- | Result type for acceptors (e.g. finite state machines and Turing machines).
--   The computation of an acceptor (e.g. a finite state machine) may conclude
--   in one of three ways:
--   1. The acceptor completes the computation in an accepting state (or set of
--      states in case of a non-deterministic acceptor).
--   2. The acceptor completes the computation in a rejecting state.
--   3. The computation is prematurely terminated (e.g. if the state transition
--      is undefined in a finite state machine).
data Acceptance sigma state
    = Accept state
    | Reject state
    | Terminated (List sigma) state