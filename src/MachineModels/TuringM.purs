module MachineModels.TuringM where

import Prelude
import Common.Auxiliary (Triple(..), mapRight)
import Data.Either (Either(..))
import Data.List (List(..), (:), dropWhile, reverse)
import Data.Map as M
import Data.Maybe (Maybe(..))
import Data.Set as S
import Data.Tuple (Tuple(..), fst, snd)
import MachineModels.Common (Acceptance(..))
import MetaModel (class CompModel)

data Direction = L | R | N

-- | Basic type for representing a Turing machine.
type TM sigma state =
    { transitions :: M.Map
        (Tuple state sigma)
        (Triple state sigma Direction)
    , initialState :: state
    , blank :: sigma
    }

-- | Turing machine as an I/O device.
newtype TuringMachineCalc sigma state = TMC (TM sigma state)

-- | Turing machine as a language acceptor required accepting states.
data TuringMachineAcceptor sigma state = TMA
    { baseTm :: TM sigma state
    , acceptingStates :: S.Set state
    }

newtype ConfigTM sigma state = ConfigTM
    { leftBand :: List sigma
    , rightBand :: List sigma
    , currentSymbol :: sigma
    , currentState :: state
    }

moveReplace :: forall sigma state .
    sigma
    -> (sigma -> sigma)
    -> Direction
    -> ConfigTM sigma state
    -> ConfigTM sigma state
moveReplace blankS f d (ConfigTM con) = case d, con.leftBand, con.rightBand of
    N, _, _ -> ConfigTM $ con {currentSymbol = newSymb}
    L, (l:ls), rs -> ConfigTM $ con
        {currentSymbol = l, leftBand = ls, rightBand = newSymb:rs}
    L, _empty, rs -> ConfigTM $ con
        {currentSymbol = blankS, rightBand = newSymb:rs}
    R, ls, (r:rs) -> ConfigTM $ con
        {currentSymbol = r, rightBand = rs, leftBand = newSymb:ls}
    R, ls, _empty -> ConfigTM $ con
        {currentSymbol = blankS, leftBand = newSymb:ls }
    where
      newSymb = f $ con.currentSymbol

changeState :: forall state sigma .
    state -> ConfigTM sigma state -> ConfigTM sigma state
changeState s (ConfigTM con) = ConfigTM $ con {currentState = s}

updateTM :: forall sigma state. Ord state => Ord sigma =>
    TM sigma state -> ConfigTM sigma state -> Either (ConfigTM sigma state) (Tuple (List sigma) state)
updateTM tm (ConfigTM con) =
  case M.lookup (Tuple con.currentState con.currentSymbol) tm.transitions of
    Nothing -> Right $ Tuple
        (trim_ tm.blank $ reverse con.leftBand
            <> (con.currentSymbol : con.rightBand))
        con.currentState
    Just (Triple newState newSym dir) -> Left
        $ changeState newState
        $ moveReplace tm.blank (const newSym) dir
        $ ConfigTM con
    where
        trim_ :: forall a . Eq a => a -> List a -> List a
        trim_ a =
            dropWhile ((==) a) >>> reverse >>> dropWhile ((==) a) >>> reverse

instance (Ord state, Ord sigma) => CompModel
    (TuringMachineCalc sigma state)
    (List sigma)
    (ConfigTM sigma state)
    (List sigma)
    where
        mInit (TMC tm) (i:is) = ConfigTM
            { leftBand : Nil
            , rightBand : is
            , currentSymbol : i
            , currentState : tm.initialState
            }
        mInit (TMC tm) _empty = ConfigTM
            { leftBand : Nil
            , rightBand : Nil
            , currentSymbol : tm.blank
            , currentState : tm.initialState
            }

        mUpdate (TMC tm) = updateTM tm >>> mapRight fst

instance (Ord state, Ord sigma) => CompModel
    (TuringMachineAcceptor sigma state)
    (List sigma)
    (ConfigTM sigma state)
    (Acceptance sigma state)
    where
        mInit (TMA tm) (i:is) = ConfigTM
            { leftBand : Nil
            , rightBand : is
            , currentSymbol : i
            , currentState : tm.baseTm.initialState
            }
        mInit (TMA tm) _empty = ConfigTM
            { leftBand : Nil
            , rightBand : Nil
            , currentSymbol : tm.baseTm.blank
            , currentState : tm.baseTm.initialState
            }

        mUpdate (TMA tm) = updateTM tm.baseTm >>> mapRight (snd >>> assign)
            where
                assign :: state -> Acceptance sigma state
                assign q = if q `S.member` tm.acceptingStates
                    then Accept q
                    else Reject q