module MachineModels.Printing where

import Prelude
import Common.Auxiliary (char2Str, id, Triple(..))
import Data.Foldable (intercalate)
import Data.List (List)
import Data.Map as M
import Data.Set as S
import Data.Tuple (Tuple(..))
import MachineModels.FSM (DFSM(..), NFSM(..))
import MachineModels.TuringM (Direction(..), TuringMachineCalc(..), TuringMachineAcceptor (..))

pPairStrChar :: Tuple String Char -> String
pPairStrChar (Tuple state symbol) = "(" <> state <> ", " <> char2Str symbol <> ")"

printInitialState :: String -> String
printInitialState s = "initial state: " <> s <> ";\n"

printAcceptingStates :: S.Set String -> String
printAcceptingStates xs = "accepting states: "
  <> "{" <> intercalate ", " xs <> "}"
  <> ";\n"

printTx :: forall a b. (a -> String) -> (b -> String) -> Tuple a b -> String
printTx f g (Tuple a b) = f a <> " -> " <> g b

printTxs :: forall a b. (a -> String) -> (b -> String) -> M.Map a b -> String
printTxs f g m = "transitions:\n  "
    <> (intercalate_ (map (printTx f g) (M.toUnfoldable m)))
    where
      intercalate_ :: List String -> String
      intercalate_ = intercalate ";\n  "

--------------------------------------------------------------------------------
--# Finite State Machines
--------------------------------------------------------------------------------

dfsmPrint :: DFSM Char String -> String
dfsmPrint (DFSM m) =
    printInitialState m.initialState
    <> printAcceptingStates m.acceptingStates
    <> printTxs pPairStrChar id m.transitions

nfsmPrint :: NFSM Char String -> String
nfsmPrint (NFSM m) =
    printInitialState m.initialState
    <> printAcceptingStates m.acceptingStates
    <> printTxs pPairStrChar printSet m.transitions
    where
      printSet :: S.Set String -> String
      printSet xs = "{" <> intercalate ", " xs <> "}"

--------------------------------------------------------------------------------
--# Turing Machines
--------------------------------------------------------------------------------

printBlank :: Char -> String
printBlank c = "blank: '" <> char2Str c <> "';\n"

printDirection :: Direction -> String
printDirection d = case d of
    L -> "L"
    R -> "R"
    N -> "N"

printTail :: Triple String Char Direction -> String
printTail (Triple state sym dir) =
    "(" <> state <> ", " <> char2Str sym <> ", " <> printDirection dir <> ")"

tmCalcPrint :: TuringMachineCalc Char String -> String
tmCalcPrint (TMC m) =
    printInitialState m.initialState
    <> printBlank m.blank
    <> printTxs pPairStrChar printTail m.transitions

tmAccPrint :: TuringMachineAcceptor Char String -> String
tmAccPrint (TMA m) =
    printInitialState m.baseTm.initialState
    <> printAcceptingStates m.acceptingStates
    <> printBlank m.baseTm.blank
    <> printTxs pPairStrChar printTail m.baseTm.transitions

